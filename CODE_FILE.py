#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
get_ipython().system('{sys.executable} -m pip install matplotlib')


# In[1]:


'pip install --upgrade pip'


# # 4-hydroxybenzaldehyde New Data Box Plots - SCN2

# # SCN2 - 3dpi

# In[1]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

S54_T = [18.58,19,18.58,19.25,18.37]
S54_C = [18.29,18.24,18.4,18.47,18.51]
S67_T = [18.08,17.67,18.01,17.76]
S67_C = [17.6,17.98,17.76]
data = list([S54_T,S54_C,S67_T,S67_C])
box = plt.boxplot(data, showmeans = True,patch_artist = True, widths=0.75)
for median in box['medians']:
    median.set(color='k', linewidth = 1.5)
colors = ['Magenta','Green','c','Orange']
for patch, color in zip(box['boxes'],colors):
    patch.set_facecolor(color)
xlabels = plt.xticks([1,2,3,4],['S54_T','S54_C','S67_T','S67_C'], fontsize = 12,fontname = "Times New Roman")
plt.yticks(fontsize = 12,fontname = "Times New Roman")
plt.ylabel("Log2 Transformed Peak Area", fontsize=12,fontname = "Times New Roman")
plt.xlabel("Samples", fontsize=12,fontname = "Times New Roman")
plt.title("SCN2_3dpi", fontname = "Times New Roman", fontsize = 14, color = 'k')
plt.grid(True)

# plt.savefig("Desktop//4HBA_Boxplots//SCN2//SCN2_3dpi.pdf", dpi=300)
plt.show()


# # SCN2 - 5dpi 

# In[3]:


S54_T = [18.61,18.75,19.21,18.86,18.55,19.03]
S54_C = [18.95,18.68,18.29,18.77,18.04,18.25]
S67_T = [18.32,18.30,18.08]
S67_C = [18.30,18.52,18.41,18.71]
data = list([S54_T,S54_C,S67_T,S67_C])
box = plt.boxplot(data, showmeans = True,patch_artist = True, widths=0.75)
for median in box['medians']:
    median.set(color='k', linewidth = 1.5)
colors = ['Magenta','Green','c','Orange']
for patch, color in zip(box['boxes'],colors):
    patch.set_facecolor(color)
xlabels = plt.xticks([1,2,3,4],['S54_T','S54_C','S67_T','S67_C'], fontsize = 12,fontname = "Times New Roman")
plt.yticks(fontsize = 12,fontname = "Times New Roman")
plt.ylabel("Log2 Transformed Peak Area", fontsize=12,fontname = "Times New Roman")
plt.xlabel("Samples", fontsize=12,fontname = "Times New Roman")
plt.title("SCN2_5dpi", fontname = "Times New Roman", fontsize = 14, color = 'k')
plt.grid(True)

#plt.savefig("Desktop//4HBA_Boxplots//SCN2//SCN2_5dpi.pdf", dpi=300)
plt.show()


# # SCN2 - 8dpi

# In[4]:


S54_T = [18.30,18.32,18.31,18.43,18.15]
S54_C = [18,18.51,18.63,18.79,18.6]
S67_T = [17.57,17.66,17.22,17.20]
S67_C = [16.63,17.76,16.79]
data = list([S54_T,S54_C,S67_T,S67_C])
box = plt.boxplot(data, showmeans = True,patch_artist = True, widths=0.75)
for median in box['medians']:
    median.set(color='k', linewidth = 1.5)
colors = ['Magenta','Green','c','Orange']
for patch, color in zip(box['boxes'],colors):
    patch.set_facecolor(color)
xlabels = plt.xticks([1,2,3,4],['S54_T','S54_C','S67_T','S67_C'], fontsize = 12,fontname = "Times New Roman")
plt.yticks(fontsize = 12,fontname = "Times New Roman")
plt.ylabel("Log2 Transformed Peak Area", fontsize=12,fontname = "Times New Roman")
plt.xlabel("Samples", fontsize=12,fontname = "Times New Roman")
plt.title("SCN2_8dpi", fontname = "Times New Roman", fontsize = 14, color = 'k')
plt.grid(True)

#plt.savefig("Desktop//4HBA_Boxplots//SCN2//SCN2_8dpi.pdf", dpi=300)
plt.show()


# # SCN2 - All (3, 5 and 8) dpi

# In[5]:


S54_3_T = [18.58,19,18.58,19.25,18.37]
S54_3_C = [18.29,18.24,18.4,18.47,18.51]
S67_3_T = [18.08,17.67,18.01,17.76]
S67_3_C = [17.6,17.98,17.76]
S54_5_T = [18.61,18.75,19.21,18.86,18.55,19.03]
S54_5_C = [18.95,18.68,18.29,18.77,18.04,18.25]
S67_5_T = [18.32,18.30,18.08]
S67_5_C = [18.30,18.52,18.41,18.71]
S54_8_T = [18.30,18.32,18.31,18.43,18.15]
S54_8_C = [18,18.51,18.63,18.79,18.6]
S67_8_T = [17.57,17.66,17.22,17.20]
S67_8_C = [16.63,17.76,16.79]
data = list([S54_3_T,S54_3_C,S67_3_T,S67_3_C,S54_5_T,S54_5_C,S67_5_T,S67_5_C,S54_8_T,S54_8_C,S67_8_T,S67_8_C])
box = plt.boxplot(data, showmeans = True,patch_artist = True, widths=0.75)
for median in box['medians']:
    median.set(color='k', linewidth = 1.5)
colors = ['Magenta','Green','c','Orange','Magenta','Green','c','Orange','Magenta','Green','c','Orange']
for patch, color in zip(box['boxes'],colors):
    patch.set_facecolor(color)
xlabels = plt.xticks([1,2,3,4,5,6,7,8,9,10,11,12],['S54_T','S54_C','S67_T','S67_C','S54_T','S54_C','S67_T','S67_C','S54_T','S54_C','S67_T','S67_C'], fontsize = 7,fontname = "Times New Roman")
plt.yticks(fontsize = 12,fontname = "Times New Roman")
plt.ylabel("Log2 Transformed Peak Area", fontsize=12,fontname = "Times New Roman")
plt.xlabel("Samples", fontsize=12,fontname = "Times New Roman")
plt.title("SCN2_All_dpi", fontname = "Times New Roman", fontsize = 14, color = 'k')
plt.grid(True)

#plt.savefig("Desktop//4HBA_Boxplots//SCN2//SCN2_All_dpi.pdf", dpi=300)
plt.show()


# # 4-hydroxybenzaldehyde New Data Box Plots - SCN5

# #  SCN5 - 3dpi

# In[6]:


S54_T = [17.48,17.62,17.76]
S54_C = [17.05,17.20,17.34]
S67_T = [16.59,17.13,16.70]
S67_C = [16.80,16.70,17.03]
data = list([S54_T,S54_C,S67_T,S67_C])
box = plt.boxplot(data, showmeans = True,patch_artist = True, widths=0.75)
for median in box['medians']:
    median.set(color='k', linewidth = 1.5)
colors = ['Magenta','Green','c','Orange']
for patch, color in zip(box['boxes'],colors):
    patch.set_facecolor(color)
xlabels = plt.xticks([1,2,3,4],['S54_T','S54_C','S67_T','S67_C'], fontsize = 12,fontname = "Times New Roman")
plt.yticks(fontsize = 12,fontname = "Times New Roman")
plt.ylabel("Log2 Transformed Peak Area", fontsize=12,fontname = "Times New Roman")
plt.xlabel("Samples", fontsize=12,fontname = "Times New Roman")
plt.title("SCN5_3dpi", fontname = "Times New Roman", fontsize = 14, color = 'k')
plt.grid(True)

#plt.savefig("Desktop//4HBA_Boxplots//SCN5//SCN5_3dpi.pdf", dpi=300)
plt.show()


# #  SCN5 - 5dpi

# In[8]:


S54_T = [17.28,17.00,17.46]
S54_C = [17.19,17.13,17.33]
S67_T = [17.20,17.35,16.93]
S67_C = [17.30,16.86,17.22]
data = list([S54_T,S54_C,S67_T,S67_C])
box = plt.boxplot(data, showmeans = True,patch_artist = True, widths=0.75)
for median in box['medians']:
    median.set(color='k', linewidth = 1.5)
colors = ['Magenta','Green','c','Orange']
for patch, color in zip(box['boxes'],colors):
    patch.set_facecolor(color)
xlabels = plt.xticks([1,2,3,4],['S54_T','S54_C','S67_T','S67_C'], fontsize = 12,fontname = "Times New Roman")
plt.yticks(fontsize = 12,fontname = "Times New Roman")
plt.ylabel("Log2 Transformed Peak Area", fontsize=12,fontname = "Times New Roman")
plt.xlabel("Samples", fontsize=12,fontname = "Times New Roman")
plt.title("SCN5_5dpi", fontname = "Times New Roman", fontsize = 14, color = 'k')
plt.grid(True)

#plt.savefig("Desktop//4HBA_Boxplots//SCN5//SCN5_5dpi.pdf", dpi=300)
plt.show()


# #  SCN5 - 8dpi

# In[9]:


S54_T = [16.92,17.23,17.26]
S54_C = [16.81,16.68,16.64]
S67_T = [16.93,17.10,16.30]
S67_C = [16.78,16.84]
data = list([S54_T,S54_C,S67_T,S67_C])
box = plt.boxplot(data, showmeans = True,patch_artist = True, widths=0.75)
for median in box['medians']:
    median.set(color='k', linewidth = 1.5)
colors = ['Magenta','Green','c','Orange']
for patch, color in zip(box['boxes'],colors):
    patch.set_facecolor(color)
xlabels = plt.xticks([1,2,3,4],['S54_T','S54_C','S67_T','S67_C'], fontsize = 12,fontname = "Times New Roman")
plt.yticks(fontsize = 12,fontname = "Times New Roman")
plt.ylabel("Log2 Transformed Peak Area", fontsize=12,fontname = "Times New Roman")
plt.xlabel("Samples", fontsize=12,fontname = "Times New Roman")
plt.title("SCN5_8dpi", fontname = "Times New Roman", fontsize = 14, color = 'k')
plt.grid(True)

#plt.savefig("Desktop//4HBA_Boxplots//SCN5//SCN5_8dpi.pdf", dpi=300)
plt.show()


# #  SCN5 - All (3, 5 and 8) dpi

# In[10]:


S54_3_T = [17.48,17.62,17.76]
S54_3_C = [17.05,17.20,17.34]
S67_3_T = [16.59,17.13,16.70]
S67_3_C = [16.80,16.70,17.03]
S54_5_T = [17.28,17.00,17.46]
S54_5_C = [17.19,17.13,17.33]
S67_5_T = [17.20,17.35,16.93]
S67_5_C = [17.30,16.86,17.22]
S54_8_T = [16.92,17.23,17.26]
S54_8_C = [16.81,16.68,16.64]
S67_8_T = [16.93,17.10,16.30]
S67_8_C = [16.78,16.84]
data = list([S54_3_T,S54_3_C,S67_3_T,S67_3_C,S54_5_T,S54_5_C,S67_5_T,S67_5_C,S54_8_T,S54_8_C,S67_8_T,S67_8_C])
box = plt.boxplot(data, showmeans = True,patch_artist = True, widths=0.75)
for median in box['medians']:
    median.set(color='k', linewidth = 1.5)
colors = ['Magenta','Green','c','Orange','Magenta','Green','c','Orange','Magenta','Green','c','Orange']
for patch, color in zip(box['boxes'],colors):
    patch.set_facecolor(color)
xlabels = plt.xticks([1,2,3,4,5,6,7,8,9,10,11,12],['S54_T','S54_C','S67_T','S67_C','S54_T','S54_C','S67_T','S67_C','S54_T','S54_C','S67_T','S67_C'], fontsize = 7,fontname = "Times New Roman")
plt.yticks(fontsize = 12,fontname = "Times New Roman")
plt.ylabel("Log2 Transformed Peak Area", fontsize=12,fontname = "Times New Roman")
plt.xlabel("Samples", fontsize=12,fontname = "Times New Roman")
plt.title("SCN5_All_dpi", fontname = "Times New Roman", fontsize = 14, color = 'k')
plt.grid(True)

#plt.savefig("Desktop//4HBA_Boxplots//SCN5//SCN5_All_dpi.pdf", dpi=300)
plt.show()

